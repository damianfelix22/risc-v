# ARQUITECTURA DE COMPUTADORAS
Alumnos: Felix Damian y Salomone Lucas

Docentes: Andres Airabella y Mauricio Palavecino

Año: 2019

## TRABAJO FINAL

El trabajo consiste en implementar un procesador en VHDL y comprobar el funcionamiento del Set de Instrucciones de RISC-V sobre el mismo.

Se comenzó por dibujar el datapath completo para el Set de Instrucciones provisto por la cátedra, teniendo en cuenta el tamaño de los buses y las 
señales intermedias que luego serán utilizados en el código VHDL. El resultado es el siguiente:

![](DATAPATH.jpg)

Basándonos en el datapath anterior, empezamos a modificar los componentes del microprocesador. Hicimos modificaciones en la memoria de programa y en la 
unidad de control. En la primera eliminamos un latch para que los saltos se hicieran de forma correcta y en la segunda quitamos y agregamos señales 
debido a que nuestro datapath es distinto al implementado en el libro. Las señales de control se muestran a continuación:

![](TABLA.jpg)

Una vez modificados estos componentes, los instanciamos en nuestro microprocesador y le asignamos señales de entrada y salida a los mismos dependiendo 
cómo se conectaban según nuestro datapath.

El paso siguiente fue testear que nuestro microprocesador corriera todas las instrucciones de risc-v, esto lo hicimos a través de la herramienta coco 
testbench. Para ello, escribimos un código en assembly y se lo pasamos a la memoria de programa en formato binario para que se ejecuten. El código es el
siguiente:

--En primer lugar, probamos la instrucción addi

ADDI x1,x0,08

--Luego, probamos el add a partir del registro x1 que cargamos anteriormente

ADD x2,x0,x1

--De esta forma, x1 y x2 quedaron cargados con el valor 01, ya que el ADD hizo la suma entre los contenidos de x0 y x1, es decir, 0+1=1

--Para probar las instrucciones LOAD y STORE primero cargo los registros x5 y x6

ADDI x5,x0,01

ADDI x6,x0,02

--Ahora hago un store dobleword y guardo en memoria el contenido de x5

SD x5,40(x6)

--Para comprobar que lo que tenía x5 se guardo en memoria, ahora lo traigo a un registro con un load dobleword

LD x3,40(x6)

--Finalmente, para ver el contenido de este registro x3, hago una suma con el registro x0 y lo guardo en x4, entonces

ADD x4,x0,x3

--A continuación, probamos que anduvieran los branch.

BEQ x5,x6,30

--Esta instruccion compara los contenidos de x5 y x6. Como los contenidos no son iguales, entonces el programa no saltó. Esto lo comprobamos ya que se 
ejecutó la instrucción siguiente, en este caso

ADDI x1,x0,08.

--Ahora probamos un caso de dos registros cuyos contenidos sí sean iguales.

BEQ x1,x2,08

--La instrucción siguiente es 

ADDI x1,x0,02 

--y fue salteada en la ejecución. Una vez saltada la instrucción, el programa siguió con el curso que traía. Esto lo comprobamos haciendo 

ADDI x1,x0,2032

--También probamos el funcionamiento de los JAL y JALR

JAL x1,08

--Esta instrucción salta directamente a PC+08. También pusimos una instrucción en medio del salto para ver que no se ejecutó. Esta instrucción es 

ADDI x1,x0,08

--Luego el programa siguió con su curso normal

--Ahora comprobamos la instruccion JALR

JALR x4,64(x0)

--Al igual que en el JAL, pusimos una instrucción en el medio del salto para ver que no se ejecutara

ADDI x1,x0,08

--Luego el programa siguió con su curso normal

--Probamos la instrucción LUI

LUI x4,0x85

--Para verificar que funcione correctamente, agregamos la siguiente instrucción y comprobamos que el resultado se corresponda con el esperado

ADDI x1,x4,01

--Por último, probamos la instrucción

SLLI x5,x6,03

--y para verificar que el desplazamiento se hizo correctamente, usamos

ADDI x1,x5,01

--Lo mismo hicimos con todas las instrucciones restantes:

SUB x1,x5,x6

LW x3,06(x1)

LWU x3,06(x1)

SW x6,00(x5)

LH x3,06(x1)

LHU x3,06(x1)

SH x6,00(x5)

LB x3,06(x1)

LBU x3,06(x1)

SB x6,00(x5)

AND x1,x5,x6

OR x1,x5,x6

XOR x1,x5,x6

ANDI x1,07(x5)

ORI x1,07(x5)

XORI x1,07(x5)

SRLI x1,02(x5)

SRAI x1,02(x5)

SLL x1,x5,x6

SRL x1,x5,x6

SRA x1,x5,x6

