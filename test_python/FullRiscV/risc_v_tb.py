import cocotb
from cocotb.clock import Clock
from cocotb.triggers import Timer, RisingEdge, FallingEdge, Edge, Event
from cocotb.result import TestFailure, TestError, ReturnValue, SimFailure

CLK_PERIOD = 10

@cocotb.coroutine
def Reset(dut):
    dut.RST_i <=  0
    yield Timer(CLK_PERIOD * 1)
    dut.RST_i  <= 1
    yield Timer(CLK_PERIOD * 10)
    dut.RST_i  <= 0

@cocotb.test()
def test(dut):
    """
    Description:
        Test RISC-V Simple Implementation
    """
    cocotb.fork(Clock(dut.CLK_i, CLK_PERIOD).start())
    
    yield Reset(dut)

    #yield Timer(CLK_PERIOD * 64)

    for i in range(64):
        yield RisingEdge(dut.CLK_i)
        print("PC OUT: ", dut.PCOUT.value.integer)
        print("INSTRUCTION: ", dut.Instruction.value)
        print("RD1: ", dut.ReadDATA1toALU.value)
        print("RD2: ", dut.ReadDATA2toALU.value)
        print("INMEDIATO: ", dut.ImmGenOUT.value)
        print("C1: ", dut.c1.value)
        print("C2: ", dut.c2.value)
        print("C3: ", dut.c3.value)
        print("C4: ", dut.c4.value)
        print("C5: ", dut.c5.value)
        print("ALU RESULT: ", dut.ALUresult.value)
        print("ZERO: ", dut.ZERO.value)
        print("BRANCH: ", dut.Branch.value)
        print("====================================================================================================")
        