--------------------------------------------------------------------------------
-- Se escriben las librerias y paquetes
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;
use std.textio.all;
--------------------------------------------------------------------------------



entity risc_v is
    port(
        CLK_i           : in std_logic;
        RST_i           : in std_logic
        );
    end;




    -- Arquitectura del procesador
    architecture grupofs of risc_v is



--------------------------------------------------------------------------------
    -- Defino señales de control (ver pag 256)
--------------------------------------------------------------------------------
    -- Señales para controlar la lectura y escrituras en el archivo de registros y memoria de datos (ver pag 257)
    signal RegWrite: std_logic;
    signal MemRead: std_logic;
    signal MemWrite: std_logic;
    -- Señales para controlar los multiplexores (ver pag 257)
	 signal c1: std_logic;
	 signal c2: std_logic;
	 signal c3: std_logic;
	 signal c4: std_logic;
         signal c5: std_logic;
-- signal ALUSrc: std_logic;
-- signal MemtoReg: std_logic;
    --Señal para posible rama
    signal Branch:std_logic;
    --Señal para el control de la ALU
    signal ALUOp: std_logic_vector(3 downto 0);
--------------------------------------------------------------------------------






--------------------------------------------------------------------------------
    -- Defino señales internas
    signal Instruction: std_logic_vector (31 downto 0); --INSTRUCTION MEMORY -> REGISTER BANK
    signal PCOUT: std_logic_vector (63 downto 0); --PC -> INSTRUCTION MEMORY;
    signal MUX1toPC: std_logic_vector(63 downto 0); -- Mux1 -> PC;
    signal En : std_logic:='1'; --Ask where the heck does this signal come from. It is used to enable (duh) both, the pc and the progmem
    signal ReadDATA1toALU: std_logic_vector(63 downto 0); 
    signal ReadDATA2toALU: std_logic_vector(63 downto 0);
    signal DataMEMtoMUX2: std_logic_vector(63 downto 0);
	 signal MUX2toMUX3: std_logic_vector(63 downto 0);
	 signal MUX3toREG: std_logic_vector(63 downto 0);
	 signal MUX5out: std_logic;
	 signal MUX6out: std_logic_vector(63 downto 0);
	 signal ImmGenOUT: std_logic_vector(63 downto 0);
    signal MUX4toALU: std_logic_vector(63 downto 0);
    signal ALUresult: std_logic_vector(63 downto 0);
    signal ZERO: std_logic;
	 signal AND_OUT: std_logic;
    signal SHIFTED_LEFT: std_logic_vector(63 downto 0);
    signal UC_MUX: std_logic;
    signal MUX_used_to_U_type: std_logic_vector(63 downto 0);
    --signal vector_used_to_LUI: std_logic_vector(63 downto 0);
---------------------------------------------------------------------------------











--------------------------------------------------------------------------------
    -- Declaracion de componente
--------------------------------------------------------------------------------
component UC is
    generic(
        opcode  :   integer :=  32
    );
    port(
        INSTR_i         :   in  std_logic_vector(opcode-1 downto 0);            -- instruccion de entrada
        Condbranch_o    :   out std_logic;                                      -- salto
        uncondbranch_o  :   out std_logic;                                      -- salto incondicional
        MemRead_o       :   out std_logic;                                      -- lectura de memoria
        c1              :   out std_logic;            
        c2              :   out std_logic;   
        c3              :   out std_logic;   
        c4              :   out std_logic;
        c5              :   out std_logic;   		  
        ALUop_o         :   out std_logic_vector(3 downto 0);              		-- seleccion de operacion de ALU
        MemWrite_o      :   out std_logic;                                      -- escritura de memoria
        U_type_o        :   out std_logic;                                      -- permite utilizar lui
        Reg_W_o         :   out std_logic                                       -- escritura de registro                                     
    );
end component UC;



component registerbank is
    generic(size_of_register     : integer := 64;  -- cantidad de bits registros
            amount_registers_dir : integer := 5);    -- cantidad de bits de direccionamiento a registros
    port (
        A_i     : IN     std_logic_vector(amount_registers_dir - 1 downto 0);
        B_i     : IN     std_logic_vector(amount_registers_dir - 1 downto 0);
        C_i     : IN     std_logic_vector(amount_registers_dir - 1 downto 0);
        Reg_W_i : IN     std_logic;
        RST_i   : IN     std_logic;
        CLK_i   : IN     std_logic;  -- Se propone eliminar el reloj, ya que registra las entradas y no permite que el uP funcione en un solo clock por instruccion
        W_c_i   : IN     std_logic_vector(size_of_register - 1 downto 0);
        R_a_o   : OUT    std_logic_vector(size_of_register - 1 downto 0);
        R_b_o   : OUT    std_logic_vector(size_of_register - 1 downto 0)
        );
end component registerbank;



component immgen is
    generic(len_i : integer := 32;  -- Longitud de la instruccion de entrada
            len_o : integer := 64);  -- Longitud de la instruccion de salida
    port (
        Inst_i : IN     std_logic_vector(len_i - 1 downto 0);  -- Instruccion de entrada
        Inmed_o : OUT    std_logic_vector(len_o - 1 downto 0)  -- Inmediato de salida
    );
end component immgen;



component datamem is
    generic(ADDR_WIDTH : integer := 10;
        MEMO_SIZE  : integer := 1024;
        DATA_WIDTH : integer := 64);
    port (
        CLK_i      : in  std_logic;
        ADDR_i     : in  std_logic_vector (ADDR_WIDTH-1 downto 0);
        DATA_i     : in  std_logic_vector (DATA_WIDTH-1 downto 0);
        DATA_o     : out std_logic_vector (DATA_WIDTH-1 downto 0);
        MemWrite   : in  std_logic;
        MemRead    : in  std_logic
    );
end component datamem;



component alu is
    generic(N : integer :=64);
    port (
        A_i      : in  std_logic_vector(N - 1 downto 0);
        B_i      : in  std_logic_vector(N - 1 downto 0);
        OUTPUT_o : out std_logic_vector(N - 1 downto 0);
        ALUop_i  : in  std_logic_vector(3 downto 0);
        Zero_o   : out std_logic
    );
end component alu;


component programcounter is
    generic (DATA_SIZE: integer :=32);
    port (
        DATA_i   : in  std_logic_vector(DATA_SIZE-1 downto 0);
        EN_i     : in  std_logic;
        RST_i    : in  std_logic;
        CLK_i    : in  std_logic;
        DATA_o   : out std_logic_vector(DATA_SIZE-1 downto 0)
    );
end component programcounter;



component progmem is
    generic(
        ancho_inst:     integer := 8;
        ancho_address:  integer := 8
    );
    port (
        CLK_i:  in  std_logic;
        RST_i:    in   std_logic;
        ADDR_i: in   std_logic_vector(ancho_address-1 downto 0);
        DATA_o: out std_logic_vector((4*ancho_inst)-1 downto 0)
    );
end component progmem;

  







--------------------------------------------------------------------------------
  begin

        PC: programcounter generic map (DATA_SIZE => 64)
        port map (

                Data_i => MUX1toPC,
                EN_i => '1',
                RST_i =>RST_i,
                CLK_i =>CLK_i,
                Data_o => PCOUT

                 );



        PM: ProgMem generic map (
                ancho_inst    => 8,
                ancho_address => 8
                )
                port map(
                RST_i => RST_i,
                CLK_i => CLK_i,
                ADDR_i => PCOUT (7 downto 0),
                Data_o => Instruction

            );



        ControlUnit: UC
        generic map(opcode => 32)
        port map(

            INSTR_i => Instruction(31 downto 0), --Ask why in the UC description file, the INSTR_i is a 32 bit vector rather than a 7 bit vector
            Condbranch_o => Branch,
            MemRead_o => MemRead,
				c1 => c1,		
				c2 => c2,	
				c3 => c3,	
				c4 => c4,
                                c5 => c5,					
            ALUop_o =>  ALUOp, --Ask why in the UC description file, the ALUop_o is a 4 bit vector rather than a 2 bit vector
            MemWrite_o => MemWrite,
            Reg_W_o => RegWrite,
            U_type_o => UC_MUX
            );

 		  

        Registers: registerbank
        generic map(
            size_of_register  => 64,  -- cantidad de bits registros
            amount_registers_dir => 5    -- cantidad de bits de direccionamiento a registros
            )
        port map(

            A_i => Instruction(19 downto 15),
            B_i => Instruction(24 downto 20),
            C_i => Instruction(11 downto 7),
            Reg_W_i => RegWrite,
            RST_i =>  RST_i,
            CLK_i =>  CLK_i,
            W_c_i => MUX_used_to_U_type,
            R_a_o => ReadDATA1toALU,
            R_b_o => ReadDATA2toALU

            );



        ImmediateGenerator: ImmGen
        generic map(
            len_i => 32,  -- Longitud de la instruccion de entrada
            len_o => 64
            )
        port map(
            Inst_i => Instruction(31 downto 0),
            Inmed_o => ImmGenOUT (63 downto 0)
            );




        ALU1: alu
        generic map(N => 64)
        port map (

            A_i => ReadDATA1toALU,
            B_i => Mux4toALU,
            OUTPUT_o => ALUresult(63 downto 0),
            ALUop_i => ALUOp,
            Zero_o =>  ZERO
            );



        DataMemory: datamem
        generic map (
            ADDR_WIDTH => 10,
            MEMO_SIZE  => 1024,
            DATA_WIDTH => 64
                )
        port map(

                CLK_i => CLK_i,
                ADDR_i => ALUresult(9 downto 0),
                DATA_i => ReadDATA2toALU,
                DATA_o => DataMEMtoMux2,
                MemWrite => MemWrite,
                MemRead => MemRead
            );


SHIFTED_LEFT <= std_logic_vector(shift_left(unsigned(ImmGenOUT), 2));
Mux4toALU <= ImmGenOUT when c3 = '1' else ReadDATA2toALU;
AND_OUT <= Branch and ZERO;
Mux5out <= AND_OUT when c5 = '0' else (ZERO);
Mux6out <= std_logic_vector(unsigned(PCOUT) + 4) when Mux5out = '0' else std_logic_vector(unsigned(ImmGenOUT)+unsigned(PCOUT));
MUX2toMUX3 <= ALUresult when c1 = '0' else DataMEMtoMux2;
MUX3toReg <= MUX2toMUX3 when c2 = '0' else std_logic_vector(unsigned(PCOUT) + 4);
MUX_used_to_U_type <= MUX3toReg when UC_MUX='0' else ImmGenOUT;
Mux1toPC <= Mux6out when c4 = '1' else ALUresult;    
	 
end architecture grupofs;